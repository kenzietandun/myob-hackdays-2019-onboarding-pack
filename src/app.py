#!/usr/bin/env python3

from flask import Flask, render_template, request, send_from_directory
import os
import template
 
app = Flask(__name__)
 
@app.route('/', methods=['GET', 'POST'])
def index():
    helptext = {}
    selections = {
            "apps":  "Apps",
            "banking":  "Banking",
            "eofy":  "End of Period",
            "payroll":  "Payroll",
            "purchases":  "Purchases",
            "sales": "Sales"
            }

    if request.method == 'POST':
        if request.form.get('submit') == 'submit':
            selected = request.form.getlist('check')
            if "all" in selected:
                return render_template("help.html", printall = True)
            return render_template("help.html", selections = selected)
    return render_template("index.html", selections = selections)

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.png', mimetype='image/vnd.microsoft.icon')

